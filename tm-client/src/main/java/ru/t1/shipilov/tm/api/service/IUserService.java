package ru.t1.shipilov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shipilov.tm.enumerated.Role;
import ru.t1.shipilov.tm.dto.model.UserDTO;

public interface IUserService extends IService<UserDTO> {

    @Nullable
    UserDTO create(@Nullable String login, @Nullable String password);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @NotNull
    UserDTO findByLogin(@Nullable String login);

    @Nullable
    UserDTO findByEmail(@Nullable String email);

    @Nullable
    UserDTO removeByLogin(@Nullable String login);

    @Nullable
    UserDTO removeByEmail(@Nullable String email);

    @NotNull
    UserDTO setPassword(@Nullable String id, @Nullable String password);

    @NotNull
    UserDTO updateUser(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    );

    boolean isLoginExist(@Nullable String login);

    boolean isEmailExist(@Nullable String email);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

}
