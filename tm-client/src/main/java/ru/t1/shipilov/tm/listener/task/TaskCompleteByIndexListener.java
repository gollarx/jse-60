package ru.t1.shipilov.tm.listener.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.shipilov.tm.dto.request.TaskCompleteByIndexRequest;
import ru.t1.shipilov.tm.event.ConsoleEvent;
import ru.t1.shipilov.tm.util.TerminalUtil;

@Component
public final class TaskCompleteByIndexListener extends AbstractTaskListener {

    @NotNull
    private final String NAME = "task-complete-by-index";

    @NotNull
    private final String DESCRIPTION = "Complete task by index.";

    @Override
    @SneakyThrows
    @EventListener(condition = "@taskCompleteByIndexListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[COMPLETE TASK BY INDEX]");
        System.out.println("ENTER INDEX");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final TaskCompleteByIndexRequest request = new TaskCompleteByIndexRequest(getToken());
        request.setIndex(index);
        taskEndpoint.completeTaskByIndex(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
