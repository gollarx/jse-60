package ru.t1.shipilov.tm.exception.user;

public final class ExistsLoginException extends AbstractUserException {

    public ExistsLoginException() {
        super("Error! Login already exists...");
    }

}
